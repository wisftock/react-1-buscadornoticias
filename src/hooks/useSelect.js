import React, { useState } from 'react';

const useSelect = (stateInitial, opciones) => {
  const [state, updateState] = useState(stateInitial);
  const handleOnChange = (e) => {
    updateState(e.target.value);
  };
  const SelectNoticias = () => {
    return (
      <select
        className='browser-default'
        value={state}
        onChange={handleOnChange}
      >
        {opciones.map((opcion) => {
          return (
            <option key={opcion.value} value={opcion.value}>
              {opcion.label}
            </option>
          );
        })}
      </select>
    );
  };
  return [state, SelectNoticias];
};

export default useSelect;
// b72aaab09bba4e5895afdc122869755a
