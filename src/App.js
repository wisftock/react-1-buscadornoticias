import { Fragment, useEffect, useState } from 'react';
import Formulario from './components/Formulario';
import Header from './components/Header';
import ListadoNoticias from './components/ListadoNoticias';

function App() {
  const [category, setCategory] = useState('');
  const [noticias, setNoticias] = useState([]);

  useEffect(() => {
    const consultarAPI = async () => {
      const url = `https://newsapi.org/v2/top-headlines?country=de&category=${category}&apiKey=b72aaab09bba4e5895afdc122869755a`;

      const respuesta = await fetch(url);
      const data = await respuesta.json();
      setNoticias(data.articles);
    };
    consultarAPI();
  }, [category]);
  return (
    <Fragment>
      <Header titulo='Buscador de noticias' />
      <div className='container white'>
        <Formulario setCategory={setCategory} />
        <ListadoNoticias noticias={noticias} />
      </div>
    </Fragment>
  );
}

export default App;
